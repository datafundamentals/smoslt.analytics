/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.analytics module, one of many modules that belongs to smoslt

 smoslt.analytics is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.analytics is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.analytics in the file smoslt.analytics/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.analytics;

//import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import smoslt.given.ParseDelimitedFile;
import smoslt.util.ConfigProperties;

public class ScoreSpreadsheetWriterTest {
	@Before
	public void setUp() {
		/*
		 * Prevents an error on the build server from no generated folder being
		 * created first
		 */
		File generated = new File(ConfigProperties.get().getProperty(
				ConfigProperties.GENERATE_FOLDER));
		if (!generated.exists()) {
			generated.mkdirs();
		}
	}

	@Test
	public void testGo() {
		SpreadsheetConfiguration spreadsheetConfiguration = SpreadsheetTestHarnessConfigurationFactory
				.getInstance().get("yada");
		try {
			ParseDelimitedFile parseDelimitedFile = new ParseDelimitedFile();
			List<List<String>> fieldValuesLists = parseDelimitedFile
					.get(SpreadsheetConfiguration.getCsvOutputFilePath());
		} catch (Exception e) {
			System.err.println("EXPECTED ON JENKINS: " + e.getMessage());
		}
		// new ScoreSpreadsheetWriter(spreadsheetConfiguration,
		// fieldValuesLists).go();
	}

}
