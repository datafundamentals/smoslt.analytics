package smoslt.analytics;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import smoslt.domain.ScoreKey;
import smoslt.domain.ScoreMunger;
import smoslt.domain.ScoreSheetTab;
import smoslt.given.ParseDelimitedFile;

public class FilteredFieldValueListsTest {
	SpreadsheetConfiguration spreadsheetConfiguration = SpreadsheetTestHarnessConfigurationFactory
			.getInstance().get("yada");

	// @Test
	// public void testGet() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testGetFailedFieldValuesLists() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testIsFailure() {
	// fail("Not yet implemented");
	// }
	//
	// @Test
	// public void testHoursWithinLimit() {
	// fail("Not yet implemented");
	// }

	@Test
	public void testGetNewFieldValues() {
		List<List<String>> fieldValuesLists = getSampleFieldValuesList();
		FilteredFieldValueLists filteredFieldValueLists = new FilteredFieldValueLists(
				fieldValuesLists, spreadsheetConfiguration,
				ScoreSheetTab.Munged);
		List<String> newFieldValues = filteredFieldValueLists
				.getNewFieldValues(fieldValuesLists.get(0));
		assertEquals("0", newFieldValues.get(0));
	}
	
	@Test
	public void testGetMungedScore() {
		List<List<String>> fieldValuesLists = getSampleFieldValuesList();
		FilteredFieldValueLists filteredFieldValueLists = new FilteredFieldValueLists(
				fieldValuesLists, spreadsheetConfiguration,
				ScoreSheetTab.Munged);
		// 11 * 1.4
		assertEquals("15", filteredFieldValueLists.getMungedScore(2, fieldValuesLists.get(0)));
		// 33 * 1.02
		assertEquals("33", filteredFieldValueLists.getMungedScore(3, fieldValuesLists.get(0)));
		// 22 * 1.3
		assertEquals("28", filteredFieldValueLists.getMungedScore(4, fieldValuesLists.get(0)));
		// 44 * 0.76
		assertEquals("33", filteredFieldValueLists.getMungedScore(5, fieldValuesLists.get(0)));
		// 55 * 0.76 but this places it below limit of 50, so instead Integer.MIN_VALUE
		assertEquals("-2147483648", filteredFieldValueLists.getMungedScore(6, fieldValuesLists.get(0)));
		// 44 * 0.76
		assertEquals("50", filteredFieldValueLists.getMungedScore(7, fieldValuesLists.get(0)));
	}


	// @Test
	// public void testGetMungededFieldValuesLists() {
	// ParseDelimitedFile parseDelimitedFile = new ParseDelimitedFile();
	// List<List<String>> fieldValuesLists = parseDelimitedFile
	// .get(spreadsheetConfiguration.getSourceFileDirectory() + "/"
	// + spreadsheetConfiguration.getSourceFilePath());
	// List<List<String>> filteredFieldValueLists = new FilteredFieldValueLists(
	// fieldValuesLists, spreadsheetConfiguration,
	// ScoreSheetTab.Munged).getMungededFieldValuesLists();
	// }

	static List<List<String>> getSampleFieldValuesList() {
		List<String> fieldValues = new ArrayList<String>();
		fieldValues.add("0");
		fieldValues.add("2345");
		fieldValues.add("11");
		fieldValues.add("22");
		fieldValues.add("33");
		fieldValues.add("44");
		fieldValues.add("55");
		fieldValues.add("66");
		fieldValues.add("true");
		fieldValues.add("true");
		fieldValues.add("true");
		fieldValues.add("true");
		fieldValues.add("true");
		fieldValues.add("true");
		fieldValues.add("true");
		fieldValues.add("true");
		fieldValues.add("true");
		fieldValues.add("true");
		fieldValues.add("true");
		fieldValues.add("true");
		List<List<String>> fieldValuesLists = new ArrayList<List<String>>();
		fieldValuesLists.add(fieldValues);
		return fieldValuesLists;
	}

}
