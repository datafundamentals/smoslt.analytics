package smoslt.analytics;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.btrg.uti.FileAsList_;

import smoslt.domain.FileConstants;
import smoslt.domain.ScoreKey;
import smoslt.domain.ScoreMunger;
import smoslt.scoremunger.Munge;
import smoslt.util.ConfigProperties;

public class GetScoreMungersTestHarness {

	public List<ScoreMunger> get() {
		System.out.println("OOOOOOOOOOOOO " + Munge.getMungeFilePath());
		File mungeFile = new File(Munge.getMungeFilePath());
		List<ScoreMunger> scoreMungers = new ArrayList<ScoreMunger>();
		List<String> mungeLines = FileAsList_.read(mungeFile);
		for (String mungeLine : mungeLines) {
			scoreMungers.add(parseMungeLine(mungeLine));
		}
		return scoreMungers;
	}

	ScoreMunger parseMungeLine(String mungeLine) {
		ScoreMunger scoreMunger = null;
		String scoreKey = null;
		String limit = null;
		String weight = null;
		int scoreLimit = 0;
		int scoreWeight = 0;
		StringTokenizer stk = new StringTokenizer(mungeLine, ",");
		int i = 0;
		while (stk.hasMoreTokens()) {
			switch (i) {
			case 0:
				scoreKey = stk.nextToken().trim();
				break;
			case 1:
				weight = stk.nextToken().trim();
				try {
					scoreWeight = Integer.valueOf(weight);
				} catch (NumberFormatException nfe) {
					throw new IllegalStateException(
							"munge file had a limit of '"
									+ limit
									+ "', which did not properly convert to an integer. See second position of line for "
									+ scoreKey);
				}
				break;
			case 2:
				limit = stk.nextToken().trim();
				try {
					scoreLimit = Integer.valueOf(limit);
				} catch (NumberFormatException nfe) {
					throw new IllegalStateException(
							"munge file had a weight of '"
									+ weight
									+ "', which did not properly convert to an integer. See third position of line for "
									+ scoreKey);

				}
				break;
			}
			i++;
		}
		scoreMunger = new ScoreMunger(scoreKey, scoreWeight, scoreLimit);
		return scoreMunger;
	}

}
