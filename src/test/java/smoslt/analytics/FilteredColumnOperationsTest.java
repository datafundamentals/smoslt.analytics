package smoslt.analytics;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import smoslt.domain.ScoreKey;
import smoslt.domain.ScoreMunger;
import smoslt.domain.ScoreSheetTab;

public class FilteredColumnOperationsTest {
	List<List<String>> fieldValuesLists;
	FilteredColumnOperations filteredColumnOperations;
	SpreadsheetConfiguration spreadsheetConfiguration = SpreadsheetTestHarnessConfigurationFactory
			.getInstance().get("yada");

	@Before
	public void setUp() {
		@SuppressWarnings("unused")
		List<List<String>> fieldValuesLists = FilteredFieldValueListsTest
				.getSampleFieldValuesList();
		filteredColumnOperations = new FilteredColumnOperations(spreadsheetConfiguration);
	}
	
	@Test
	public void testGetWeightedScore(){
		assertEquals("46",filteredColumnOperations.getWeightedScore(2, "33"));
	}


	@Test
	public void testGetIncomingScoreHeader() {
		assertEquals("FAIL", filteredColumnOperations.getIncomingScoreHeader(0));
		assertEquals("Hours", filteredColumnOperations.getIncomingScoreHeader(1));
		assertEquals("FeedbackSpeed",
				filteredColumnOperations.getIncomingScoreHeader(2));
		assertEquals("RDD", filteredColumnOperations.getIncomingScoreHeader(3));
		assertEquals("ManagerSpeak",
				filteredColumnOperations.getIncomingScoreHeader(4));
		assertEquals("ScalesUp",
				filteredColumnOperations.getIncomingScoreHeader(5));
		assertEquals("POLR", filteredColumnOperations.getIncomingScoreHeader(6));
	}


	@Test
	public void testGetMungeFieldPositionIndex() {
		assertEquals(0, filteredColumnOperations.getMungeFieldPositionIndex("FAIL"));
		try{
			assertEquals(0, filteredColumnOperations.getMungeFieldPositionIndex("Fail"));
			fail();
		}catch(
			IllegalArgumentException e){
			//expected
		}
		assertEquals(0, filteredColumnOperations.getMungeFieldPositionIndex("FAIL"));
		assertEquals(1, filteredColumnOperations.getMungeFieldPositionIndex("Hours"));
		assertEquals(2, filteredColumnOperations.getMungeFieldPositionIndex("FeedbackSpeed"));
		assertEquals(3, filteredColumnOperations.getMungeFieldPositionIndex("ManagerSpeak"));
		assertEquals(4, filteredColumnOperations.getMungeFieldPositionIndex("RDD"));
		assertEquals(5, filteredColumnOperations.getMungeFieldPositionIndex("ScalesUp"));
		assertEquals(6, filteredColumnOperations.getMungeFieldPositionIndex("POLR"));
		assertEquals(7, filteredColumnOperations.getMungeFieldPositionIndex("LongTerm"));
	}

	@Test
	public void testGetScoreMungerFieldPositionIndex() {
		assertEquals(0,
				filteredColumnOperations
						.getScoreMungerFieldPositionIndex(new ScoreMunger(
								ScoreKey.FAIL.toString(), 0, 0)));
		assertEquals(1,
				filteredColumnOperations
						.getScoreMungerFieldPositionIndex(new ScoreMunger(
								ScoreKey.Hours.toString(), 0, 0)));
		assertEquals(2,
				filteredColumnOperations
						.getScoreMungerFieldPositionIndex(new ScoreMunger(
								ScoreKey.FeedbackSpeed.toString(), 0, 0)));
		assertEquals(3,
				filteredColumnOperations
						.getScoreMungerFieldPositionIndex(new ScoreMunger(
								ScoreKey.ManagerSpeak.toString(), 0, 0)));
		assertEquals(3,
				filteredColumnOperations
						.getScoreMungerFieldPositionIndex(new ScoreMunger(
								ScoreKey.ManagerSpeak.toString(), 0, 0)));
		assertEquals(4,
				filteredColumnOperations
						.getScoreMungerFieldPositionIndex(new ScoreMunger(
								ScoreKey.RDD.toString(), 0, 0)));
		assertEquals(5,
				filteredColumnOperations
						.getScoreMungerFieldPositionIndex(new ScoreMunger(
								ScoreKey.ScalesUp.toString(), 0, 0)));
		assertEquals(6,
				filteredColumnOperations
						.getScoreMungerFieldPositionIndex(new ScoreMunger(
								ScoreKey.POLR.toString(), 0, 0)));
	}

}
