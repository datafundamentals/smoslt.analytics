package smoslt.analytics;

import java.util.List;

import smoslt.domain.FileConstants;
import smoslt.domain.ScoreMunger;
import smoslt.domain.ScoreTracker;
import smoslt.given.ScoreTrackerTestHarness;
import smoslt.given.SideEffectList;
import smoslt.given.SideEffectListTestHarness;

public class SpreadsheetTestHarnessConfigurationFactory {
	private ScoreTracker scoreTracker = new ScoreTrackerTestHarness(null);
	private SideEffectList sideEffectList = new SideEffectListTestHarness();
	private List<ScoreMunger> scoreMungers;
	private static SpreadsheetTestHarnessConfigurationFactory instance;

	public static SpreadsheetTestHarnessConfigurationFactory getInstance() {
		if (null == instance) {
			instance = new SpreadsheetTestHarnessConfigurationFactory();
		}
		return instance;
	}

	private SpreadsheetTestHarnessConfigurationFactory() {

	}

	public SpreadsheetConfiguration get(String workbookName) {
		SpreadsheetConfiguration config;
		configureSideEffectList();
		configureScoreMungers();
		config = new SpreadsheetConfiguration(scoreTracker, sideEffectList,
				scoreMungers);
		return config;
	}

	private void configureScoreMungers() {
		scoreMungers = new GetScoreMungersTestHarness().get();
	}

	private void configureSideEffectList() {
		// sideEffectList =
	}
}
