package smoslt.analytics;

import static org.junit.Assert.*;

import org.junit.Test;

public class FirstRowTest {

	@Test
	public void testGetFilteredFieldValue() {
		FirstRow firstRow = new FirstRow(null, null, null);
		String testString1 = "Abc_Blah";
		String testString2 = "Abc1_Blah";
		String testString3 = "Abc1Foo_Bleh";
		String returnValue = firstRow.getFilteredFieldValue(testString1);
		assertEquals("Blah", returnValue);
		returnValue = firstRow.getFilteredFieldValue(testString2);
		assertEquals("Blah", returnValue);
		returnValue = firstRow.getFilteredFieldValue(testString3);
		assertEquals("Abc1Foo_Bleh", returnValue);
	}

}
