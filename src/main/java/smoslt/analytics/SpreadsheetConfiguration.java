package smoslt.analytics;

import java.util.List;

import smoslt.domain.ScoreMunger;
import smoslt.domain.ScoreTracker;
import smoslt.given.SideEffectList;
import smoslt.util.ConfigProperties;

public class SpreadsheetConfiguration {
	private ScoreTracker scoreTracker;
	private SideEffectList sideEffectList;
	private List<ScoreMunger> scoreMungers;

	public SpreadsheetConfiguration(ScoreTracker scoreTracker,
			SideEffectList sideEffectList, List<ScoreMunger> scoreMungers) {
		this.scoreTracker = scoreTracker;
		this.sideEffectList = sideEffectList;
		this.scoreMungers = scoreMungers;
	}

	public ScoreTracker getScoreTracker() {
		return scoreTracker;
	}

	public SideEffectList getSideEffectList() {
		return sideEffectList;
	}

	public List<ScoreMunger> getScoreMungers() {
		return scoreMungers;
	}

	public static String getCsvOutputFilePath() {
		return ConfigProperties.get().getProperty(
				ConfigProperties.GENERATE_FOLDER)
				+ "/"
				+ ConfigProperties.get().getProperty(
						ConfigProperties.CSV_OUTPUT_DATA_FILE_NAME);

	}

	public static String getExcelFilePath() {
		return ConfigProperties.get().getProperty(
				ConfigProperties.GENERATE_FOLDER)
				+ "/"
				+ ConfigProperties.get().getProperty(
						ConfigProperties.EXCEL_OUTPUT_DATA_FILE_NAME);

	}

}
