package smoslt.analytics;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.btrg.uti.StringUtils_;

import smoslt.domain.ScoreKey;
import smoslt.domain.ScoreMunger;
import smoslt.domain.ScoreSheetTab;

public class FilteredFieldValueLists {
	private SpreadsheetConfiguration config;
	private FilteredColumnOperations filteredColumnOperations;
	List<List<String>> fieldValuesLists;
	ScoreSheetTab scoreSheetTab;

	public FilteredFieldValueLists(List<List<String>> fieldValuesLists,
			SpreadsheetConfiguration spreadsheetConfiguration,
			ScoreSheetTab scoreSheetTab) {
		this.fieldValuesLists = fieldValuesLists;
		this.config = spreadsheetConfiguration;
		this.scoreSheetTab = scoreSheetTab;
		filteredColumnOperations = new FilteredColumnOperations(config);
	}

	public List<List<String>> get() {
		if (scoreSheetTab == ScoreSheetTab.All) {
			return fieldValuesLists;
		} else if (scoreSheetTab == ScoreSheetTab.FAIL) {
			return getFailedFieldValuesLists();
		} else if (scoreSheetTab == ScoreSheetTab.Munged) {
			return getMungededFieldValuesLists();
		} else {
			throw new UnsupportedOperationException();
		}
	}

	List<List<String>> getFailedFieldValuesLists() {
		List<List<String>> failures = new ArrayList<List<String>>();
		for (List<String> fieldValues : fieldValuesLists) {
			if (isFailure(fieldValues)) {
				failures.add(fieldValues);
			}
		}
		return failures;
	}

	boolean isFailure(List<String> values) {
		int comparison = config.getScoreMungers().get(0).getScoreLimit();
		int intValue = Integer.valueOf(values.get(0));
		if (intValue > comparison) {
			return true;
		} else {
			return false;
		}
	}

	int hoursWithinLimit(List<String> values) {
		int comparison = config.getScoreMungers().get(1).getScoreLimit();
		int intValue = Math.abs(Integer.valueOf(values.get(1)));
		if (intValue <= comparison) {
			return intValue;
		} else {
			return -1;
		}
	}

	List<List<String>> getMungededFieldValuesLists() {
		List<List<String>> newFieldValueLists = new ArrayList<List<String>>();
		SortedMap<String, List<String>> sortedMap = new TreeMap<String, List<String>>();
		StringBuffer sortKey = null;
		String scoreAppender = "11111";
		int score = 0;
		boolean abort = true;
		List<String> newFieldValues;
		for (List<String> fieldValues : fieldValuesLists) {
			newFieldValues = getNewFieldValues(fieldValues);
			// print(fieldValues);
			// print(newFieldValues);
			abort = false;
			sortKey = new StringBuffer();
			int hours = hoursWithinLimit(newFieldValues);
			if (noBrokenLimits(newFieldValues) && hours > 0) {
				sortKey.append(StringUtils_.getFrontFilledFixedLengthString(""
						+ hours, '0', 5, true, true));
				for (ScoreMunger scoreMunger : config.getScoreMungers()) {
					if (scoreMunger.getScoreKey() != ScoreKey.FAIL
							&& scoreMunger.getScoreKey() != ScoreKey.Hours) {
						score = score(scoreMunger, newFieldValues);
						scoreAppender = StringUtils_
								.getFrontFilledFixedLengthString("" + score,
										'0', 5, true, true);
						if (score >= 0) {
							sortKey.append(scoreAppender);
						} else {
							abort = true;
							break;
						}
					}
				}
			} else {
				abort = true;
			}
			if (!abort) {
				// System.out.println(sortKey.toString());
				sortKey.append(getOptionFieldsAs1or0(fieldValues));
				sortedMap.put(sortKey.toString(), fieldValues);
			}
		}
		for (String key : sortedMap.keySet()) {
			newFieldValueLists.add(sortedMap.get(key));
		}
		return newFieldValueLists;
	}

	boolean noBrokenLimits(List<String> fieldValues) {
		boolean noBrokenLimits = true;
		for (String value : fieldValues) {
			if (value.equals("" + Integer.MIN_VALUE)) {
				noBrokenLimits = false;
				break;
			}
		}
		return noBrokenLimits;
	}

	List<String> getNewFieldValues(List<String> fieldValues) {
		// ? No idea what I was debugging here
		// if(!fieldValues.get(3).equals("0")){
		// System.out.println("STOP");
		// }
		List<String> newFieldValues = new ArrayList<String>();
		for (int i = 0; i < fieldValues.size(); i++) {
			if (i < 2 || i >= config.getScoreTracker().getHeaders().size()) {
				newFieldValues.add(fieldValues.get(i));
			} else {
				newFieldValues.add(getMungedScore(i, fieldValues));
			}
		}
		return newFieldValues;
	}

	String getMungedScore(int i, List<String> fieldValues) {
		int newColumnIndex = filteredColumnOperations
				.getMungeFieldPositionIndex(filteredColumnOperations
						.getIncomingScoreHeader(i));
		String newValue = fieldValues.get(newColumnIndex);
		newValue = filteredColumnOperations.getWeightedScore(newColumnIndex,
				newValue);
		return newValue;
	}

	int score(ScoreMunger scoreMunger, List<String> fieldValues) {
		int returnValue = -1;
		int fieldPositionIndex = filteredColumnOperations
				.getScoreMungerFieldPositionIndex(scoreMunger);
		try {
			returnValue = Math.abs(Integer.valueOf(fieldValues
					.get(fieldPositionIndex)));
		} catch (NumberFormatException nfe) {
			returnValue = -1;
			System.err.println("WOOPS " + fieldValues.get(fieldPositionIndex));
		}
		if (returnValue < scoreMunger.getScoreLimit()) {
			returnValue = -1;
		}
		return returnValue;
	}

	String getOptionFieldsAs1or0(List<String> fieldValues) {
		StringBuffer sb = new StringBuffer();
		for (String value : fieldValues) {
			if (value.trim().equals("true")) {
				sb.append("1");
			} else if (value.trim().equals("false")) {
				sb.append("0");
			}
		}
		return sb.toString();
	}

	static void print(List<String> fieldList) {
		System.out.print("FIELD LIST: ");
		for (String value : fieldList) {
			System.out.print(value + " ");
		}
		System.out.println();
	}

}
