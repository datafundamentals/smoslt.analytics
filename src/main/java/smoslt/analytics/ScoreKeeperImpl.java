/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.analytics module, one of many modules that belongs to smoslt

 smoslt.analytics is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.analytics is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.analytics in the file smoslt.analytics/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package  smoslt.analytics;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.btrg.uti.DateUtils_;
import org.btrg.uti.NioFileUtil;
import org.btrg.uti.StringUtils_;
import org.btrg.utils.random.RandomProvider;

import smoslt.domain.FileConstants;
import smoslt.domain.Schedule;
import smoslt.given.ScoreTrackerImpl;
import smoslt.optionsapi.Option;
import smoslt.optionsapi.Options;
import smoslt.optionsapi.ScoreKeeper;
import smoslt.stacker.StackTasks;
import smoslt.util.ConfigProperties;
import smoslt.util.ObjectCloner;

/**
 * ScoreKeeper is a singleton service and is not allowed to maintain any state.
 * It must always get new state for any set of options performed.
 * 
 * ScoreKeeper is the single point of interface between the smoslt.options
 * module and the rest of the more domain specific modules. It's job is to
 * protect the smoslt.options module from any knowledge of the smoslt project
 * specific domain knowledge.
 * 
 * The reason for this isolation is to allow the smoslt.option module to later
 * become usable with any type of options, inside or outside of the project
 * specific domain space.
 * 
 * To avoid cyclic dependencies, other smoslt modules can know about and depend
 * on smoslt.options, but smoslt.options cannot know about and depend on other
 * smoslt modules, except smoslt.optionsapi. The in-between broker is
 * smoslt.optionsapi which both can know about, and which provides a shadow
 * artifacts of the options used in the optaplanner operations. This does create
 * more object churn, but is probably not enough to affect memory footprint or
 * performance in a big way
 * 
 * As such, by practice, ScoreKeeper is only allowed to score a set of options,
 * at the API level. How it does this internally is it's own business. It may
 * have no other public API
 * 
 * @author petecarapetyan
 *
 */
public class ScoreKeeperImpl implements ScoreKeeper {
	private StackTasks stackTasks;
	private Schedule schedule;
	private Schedule scheduleClone;
	private Map<String, List<Integer>> scores = new HashMap<String, List<Integer>>();
	public static int printCount = 0;

	public ScoreKeeperImpl(StackTasks stackTasks, Schedule schedule) {
		this.stackTasks = stackTasks;
		this.schedule = schedule;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see smoslt.optionsapi.ScoreKeeper#softScore(smoslt.optionsapi.Options,
	 * int)
	 */
	@Override
	public int score(Options options, int bendableScoreIndex) {
		List<Integer> optionScores = score(options);
		int returnValue = optionScores.get(bendableScoreIndex);
		return returnValue;
	}

	private List<Integer> score(Options options) {
		String optionString = getOptionIdString(options);
		List<Integer> optionScores = new ArrayList<Integer>();
		if (!scores.containsKey(optionString)) {
			scheduleClone = null;
			try {
				scheduleClone = (Schedule) ObjectCloner.deepCopy(schedule);
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("Cloning of schedule failed with\t"
						+ e.getMessage());
			}
			optionScores = stackTasks.go(new ScoreTrackerImpl(scheduleClone), options);
			scores.put(optionString, optionScores);
			logScores(optionString, optionScores);
		} else {
			optionScores = scores.get(optionString);
		}
		return optionScores;
	}

	private void logScores(String optionString, List<Integer> optionScores) {
		StringBuffer sb = new StringBuffer();
		sb.append(optionString + ",");
		for (int i : optionScores) {
			sb.append(i + ",");
		}
//		String log = sb.toString().substring(0, sb.length()-1)+ "\n";
		String log = sb.toString()+ "\n";
		NioFileUtil.append(getCsvOutputFilePath(), log.getBytes());
	}

	private String getOptionIdString(Options options) {
		StringBuffer answer = new StringBuffer();
		for (Option onOff : options.getOptionList()) {
			if (onOff.isOnOff()) {
				answer.append('1');
			} else {
				answer.append('0');
			}
		}
		return answer.toString();
	}

	public static String getSpacerFeed() {
		printCount++;
		if (printCount == 25) {
			printCount = 0;
			return "\n";
		} else {
			return " ";
		}
	}

	public void print(Options options) {
		System.out.println("OPTIONS");
		for (Option option : options.getOptionList()) {
			System.out.println("\tOPTION " + option.getId() + " IS ON "
					+ option.isOnOff());
		}
	}
	
	public Schedule getScheduleClone(){
		return scheduleClone;
	}

	String getCsvOutputFilePath(){
		return ConfigProperties.get().getProperty(ConfigProperties.GENERATE_FOLDER)+ "/"
				+ ConfigProperties.get().getProperty(ConfigProperties.CSV_OUTPUT_DATA_FILE_NAME);
		
	}

}
