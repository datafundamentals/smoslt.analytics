/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.analytics module, one of many modules that belongs to smoslt

 smoslt.analytics is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.analytics is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.analytics in the file smoslt.analytics/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.analytics;

import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import smoslt.domain.ScoreMunger;
import smoslt.domain.ScoreSheetTab;

public class GetSheet {
	private List<List<String>> fieldValueLists;
	private Workbook wb;
	private ScoreSheetTab scoreSheetTab;
	private SpreadsheetConfiguration config;

	public GetSheet(List<List<String>> fieldValueLists, Workbook wb,
			ScoreSheetTab scoreSheetTab,
			SpreadsheetConfiguration spreadsheetConfiguration) {
		this.fieldValueLists = fieldValueLists;
		this.wb = wb;
		this.scoreSheetTab = scoreSheetTab;
		this.config = spreadsheetConfiguration;
	}

	Sheet go() {
		Sheet returnSheet = wb.createSheet("" + scoreSheetTab);
		int j = 0;
		CellStyle style = wb.createCellStyle();
		style.setFillBackgroundColor(IndexedColors.AQUA.getIndex());
		style.setFillPattern(CellStyle.BIG_SPOTS);
		// List<List<String>> filteredFieldValueLists = new
		// FilteredFieldValueLists(
		// config).get();
		new FirstRow(returnSheet, config, scoreSheetTab).go();
		j++;
		/*
		 * What needs to happen right here is loop through all rows first, and
		 * build an index of rows ordered by number of hours, so that if there
		 * are greater than 32768 rows you can lop off any rows that are going
		 * to be in the highest number of hours
		 */
		for (List<String> fieldValues : fieldValueLists) {
			if (!fieldValues.isEmpty() && j < 32768
					&& !fieldValues.get(1).equals("0")) {
				Row row = returnSheet.createRow((short) j++);
				int i = 0;
				for (String fieldValue : fieldValues) {
					if (doesThisColumnWrite(i)) {
						writeRow(row, i, fieldValue, style);
					}
					i++;
				}
			} else if (j >= 32768) {
				j++;
			}
		}
		if (j >= 32768) {
			System.err.println("BUG: RAN OUT OF SPACE TOO MANY COMPONENTS BY "
					+ (j - 32768) + " ROWS");
		}
		return returnSheet;
	}

	void writeRow(Row row, int rowIndex, String fieldValue, CellStyle style) {
		Cell cell = row.createCell(rowIndex);
		cell.setCellValue(fieldValue);
		if (fieldValue.equals("true")) {
			// Aqua background, supposedly. Just grey in
			// LibreOffice?
			cell.setCellStyle(style);
		}
	}


	boolean doesThisColumnWrite(int columnIndex) {
		return true;
	}
}
