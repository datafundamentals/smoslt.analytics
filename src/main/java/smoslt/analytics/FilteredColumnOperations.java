package smoslt.analytics;

import java.util.ArrayList;
import java.util.List;

import smoslt.domain.ScoreMunger;

/**
 * This is intended to provide a place for both column header row and the data
 * rows to re-configure columns based on the different column orders of the
 * incoming versus outgoing columns of FilteredFieldValueLists
 */
public class FilteredColumnOperations {
	SpreadsheetConfiguration config;

	public FilteredColumnOperations(
			SpreadsheetConfiguration spreadsheetConfiguration) {
		this.config = spreadsheetConfiguration;
	}

	String getIncomingScoreHeader(int columnIndex) {
		String returnValue = null;
		returnValue = ""
				+ config.getScoreTracker().getHeaders().get(columnIndex);
		return returnValue;
	}

	int getMungeFieldPositionIndex(String header) {
		int returnValue = getScoreMungerFieldPositionIndex(new ScoreMunger(
				header, 0, 0));
		return returnValue;
	}

	int getScoreMungerFieldPositionIndex(ScoreMunger scoreMunger) {
		int returnValue = 0;
		boolean found = false;
		for (ScoreMunger comparison : config.getScoreMungers()) {
			if (comparison.getScoreKey().equals(scoreMunger.getScoreKey())) {
				found = true;
				break;
			}
			returnValue++;
		}
		if (!found) {
			throw new IllegalStateException();
		}
		return returnValue;
	}

	String getWeightedScore(int columnIndex, String incomingValue) {
		ScoreMunger scoreMunger = config.getScoreMungers().get(columnIndex);
		int value = Integer.valueOf(incomingValue);
		value = value * scoreMunger.getScoreWeight() / 100;
		if (value < scoreMunger.getScoreLimit()) {
			value = Integer.MIN_VALUE;
		}
		return "" + value;
	}


}
