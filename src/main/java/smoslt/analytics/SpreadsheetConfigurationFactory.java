package smoslt.analytics;

import java.util.List;

import smoslt.domain.FileConstants;
import smoslt.domain.Schedule;
import smoslt.domain.ScoreMunger;
import smoslt.domain.ScoreTracker;
import smoslt.given.ScoreTrackerImpl;
import smoslt.given.SideEffectList;
import smoslt.given.SideEffectListImpl;
import smoslt.scoremunger.GetScoreMungersImpl;

public class SpreadsheetConfigurationFactory {
	private ScoreTracker scoreTracker;
	private SideEffectList sideEffectList = new SideEffectListImpl();
	private List<ScoreMunger> scoreMungers = new GetScoreMungersImpl().get();
	private Schedule schedule;
	private static SpreadsheetConfigurationFactory instance;

	public static SpreadsheetConfigurationFactory getInstance() {
		if (null == instance) {
			instance = new SpreadsheetConfigurationFactory();
		}
		return instance;
	}

	private SpreadsheetConfigurationFactory() {

	}

	public SpreadsheetConfiguration get(Schedule schedule, String workbookName) {
		this.scoreTracker = new ScoreTrackerImpl(schedule);
		return new SpreadsheetConfiguration(scoreTracker, sideEffectList,
				scoreMungers);
	}
}
