/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.analytics module, one of many modules that belongs to smoslt

 smoslt.analytics is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.analytics is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.analytics in the file smoslt.analytics/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.analytics;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

import smoslt.domain.FileConstants;
import smoslt.domain.ScoreMunger;
import smoslt.domain.ScoreSheetTab;
import smoslt.given.ParseDelimitedFile;
import smoslt.scoremunger.Munge;
import smoslt.util.ConfigProperties;

public class ScoreSpreadsheetWriter {
	private SpreadsheetConfiguration config;
	private List<List<String>> fieldValuesLists;
	private Workbook wb = new HSSFWorkbook();

	public ScoreSpreadsheetWriter(
			SpreadsheetConfiguration spreadsheetConfiguration,
			List<List<String>> fieldValuesLists) {
		this.config = spreadsheetConfiguration;
		this.fieldValuesLists = fieldValuesLists;
	}

	public void go() {
		FileOutputStream fileOut;
		try {
			writeSheets();
			fileOut = new FileOutputStream(
					SpreadsheetConfiguration.getExcelFilePath());
			wb.write(fileOut);
			fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	void writeSheets() {
		File mungeFile = new File(Munge.getMungeFilePath());
		if (mungeFile.exists()) {
			List<List<String>> mungedFieldValueLists = new FilteredFieldValueLists(
					fieldValuesLists, config, ScoreSheetTab.Munged).get();
			new GetSheet(mungedFieldValueLists, wb, ScoreSheetTab.Munged,
					config).go();
			List<List<String>> failedFieldValueLists = new FilteredFieldValueLists(
					fieldValuesLists, config, ScoreSheetTab.FAIL).get();
			new GetSheet(failedFieldValueLists, wb, ScoreSheetTab.FAIL, config)
					.go();
		}
		new GetSheet(fieldValuesLists, wb, ScoreSheetTab.All, config).go();
	}

}
