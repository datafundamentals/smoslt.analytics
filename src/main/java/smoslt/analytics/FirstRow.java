/*Copyright 2014 Pete Carapetyan
 This file is part of smoslt.analytics module, one of many modules that belongs to smoslt

 smoslt.analytics is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 smoslt.analytics is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with smoslt.analytics in the file smoslt.analytics/GNUGeneralPublicLicensev3.0.html  
 If not, see <http://www.gnu.org/licenses/>.
 */
package smoslt.analytics;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import smoslt.domain.ScoreKey;
import smoslt.domain.ScoreMunger;
import smoslt.domain.ScoreSheetTab;
import smoslt.domain.ScoreTracker;
import smoslt.given.ScoreTrackerImpl;
import smoslt.given.SideEffectList;
import smoslt.given.SideEffectListImpl;

public class FirstRow {
	private SpreadsheetConfiguration config;
	private Sheet returnSheet;
	private ScoreSheetTab scoreSheetTab;

	public FirstRow(Sheet returnSheet,
			SpreadsheetConfiguration spreadsheetConfiguration,
			ScoreSheetTab scoreSheetTab) {
		this.config = spreadsheetConfiguration;
		this.returnSheet = returnSheet;
		this.scoreSheetTab = scoreSheetTab;
	}

	public void go() {
		Row row = returnSheet.createRow(0);
		int i = 0;
		for (String columnHead : getColumnHeads()) {
			row.createCell(i++).setCellValue(getFilteredFieldValue(columnHead));
		}
	}

	public List<String> getColumnHeads() {
		List<String> columnHeads = new ArrayList<String>();
		if (scoreSheetTab == ScoreSheetTab.Munged) {
			for (int i = 0; i < config.getScoreMungers().size(); i++) {
				columnHeads.add(config.getScoreMungers().get(i).getScoreKey()
						.toString());
			}
		} else {
			for (ScoreKey scoreHead : config.getScoreTracker().getHeaders()) {
				columnHeads.add(scoreHead.toString());
			}
		}
		for (String sideEffect : config.getSideEffectList().get()) {
			columnHeads.add(sideEffect);
		}
		return columnHeads;
	}
	
	String getFilteredFieldValue(String fieldValue) {
		if(fieldValue.contains("_")){
			String cleanedValue = (fieldValue.substring(fieldValue.indexOf("_")+1, fieldValue.length()));
			int differenceInLength =fieldValue.length()-cleanedValue.length();
			if(differenceInLength!=4&&differenceInLength!=5){
				return fieldValue;
			}
			return cleanedValue;
		}else{
			return fieldValue;
		}
	}
}
